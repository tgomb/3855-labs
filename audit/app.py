import connexion
from connexion import NoContent

import pymysql
import yaml
import logging.config
import logging
import json
from pykafka import KafkaClient
from pykafka.common import OffsetType
from threading import Thread
from flask_cors import CORS, cross_origin
import os

if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yaml"
    log_conf_file = "/config/log_conf.yaml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yaml"
    log_conf_file = "log_conf.yaml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

# External Logging Configuration
with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)


def get_health():
    return 200


def get_order_set(index):
    """ """
    k_hostname = "%s:%d" % (app_config['events']['hostname'],
                            app_config['events']['port'])
    client = KafkaClient(hosts=k_hostname)
    topic = client.topics[str.encode(app_config['events']['topic'])]

    consumer = topic.get_simple_consumer(reset_offset_on_start=True, consumer_timeout_ms=1000)

    logger.info("Retrieving set order at index %d" % index)
    try:
        set_grocery = []
        for msg in consumer:
            msg_str = msg.value.decode('utf-8')
            msg = json.loads(msg_str)

            if msg['type'] == 'sg':
                set_grocery.append(msg)

        event = set_grocery[index]
        return event, 200

    except:
        logger.error("No more error messages found")

    logger.error("Could not find set grocery order at index %d" % index)
    return {"message": "Not Found"}, 404


def get_order_manual(index):
    k_hostname = "%s:%d" % (app_config['events']['hostname'],
                            app_config['events']['port'])
    client = KafkaClient(hosts=k_hostname)
    topic = client.topics[str.encode(app_config['events']['topic'])]

    consumer = topic.get_simple_consumer(reset_offset_on_start=True, consumer_timeout_ms=1000)

    logger.info("Retrieving manual order at index %d" % index)
    try:
        manual_grocery = []
        for msg in consumer:
            msg_str = msg.value.decode('utf-8')
            msg = json.loads(msg_str)

            if msg['type'] == 'mg':
                manual_grocery.append(msg)

        event = manual_grocery[index]
        # print(event)
        return event, 200

    except:
        logger.error("No more error messages found")

    logger.error("Could not find set grocery order at index %d" % index)
    return {"message": "Not Found"}, 404


app = connexion.FlaskApp(__name__, specification_dir='')
CORS(app.app)
app.app.config['CORS_HEADERS'] = 'Content-Type'

app.add_api("grocery.yaml", strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    app.run(port=8110)

