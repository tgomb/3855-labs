import sqlite3

conn = sqlite3.connect('stats.sqlite')

c = conn.cursor()
c.execute('''
        CREATE TABLE stats
        (id INTEGER PRIMARY KEY ASC,
        num_sg_orders INTEGER NOT NULL,
        cur_sg_order INTEGER NOT NULL,
        num_mg_orders INTEGER NOT NULL,
        cur_mg_order INTEGER NOT NULL,
        last_updated VARCHAR(100) NOT NULL)
        ''')
conn.commit()
conn.close()
