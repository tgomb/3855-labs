import connexion
from connexion import NoContent
import sqlite3

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import and_
from base import Base
import datetime
import json
import yaml
import logging.config
import logging
from apscheduler.schedulers.background import BackgroundScheduler
import requests
from stats import Stats
from flask_cors import CORS, cross_origin
import os

if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yaml"
    log_conf_file = "/config/log_conf.yaml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yaml"
    log_conf_file = "log_conf.yaml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

# External Logging Configuration
with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

database = app_config['datastore']['filename']
url = app_config['eventstore']['url']
sq_file = app_config['datastore']['filename']

# DB_ENGINE = create_engine("sqlite:///grocery.sqlite")
DB_ENGINE = create_engine(f"sqlite:///{database}")
# app_config["datastore"]["filename"]
Base.metadata.bind = DB_ENGINE
Base.metadata.create_all(DB_ENGINE)
DB_SESSION = sessionmaker(bind=DB_ENGINE)


def create_database():
    conn = sqlite3.connect(sq_file)
    c = conn.cursor()
    c.execute('''
            CREATE TABLE stats
            (id INTEGER PRIMARY KEY ASC,
            num_sg_orders INTEGER NOT NULL,
            cur_sg_order INTEGER,
            num_mg_orders INTEGER NOT NULL,
            cur_mg_order INTEGER,
            last_updated VARCHAR(100) NOT NULL)
            ''')
    conn.commit()
    conn.close()

if os.path.exists(sq_file) == False:
    create_database()


def get_health():
    return 200
    

def get_stats():
    """"""
    logger.info("Request started")
    session = DB_SESSION()
    results = session.query(Stats).order_by(Stats.last_updated.desc())
    # gives query

    con = sqlite3.connect(sq_file)
    cur = con.cursor()
    cur.execute(str(results))
    result = cur.fetchall()

    if not result:
        logger.error('No Stats Exist')
        return 404, "Statistics do not exist"
    else:
        stats = {
            "num_sg_orders": result[0][1],
            "cur_sg_order": result[0][2],
            "num_mg_orders": result[0][3],
            "cur_mg_order": result[0][4]
        }
        logger.debug(stats)
    logger.info("Request has been completed")
    con.close()
    session.close()

    return stats, 200


def populate_stats():
    """pass"""
    logger.info(f"Periodic processing has started")
    session = DB_SESSION()

    results = session.query(Stats).all()

    if not results:
        i = Stats(
            0,
            0,
            0,
            0,
            datetime.datetime.now())
        session.add(i)
        session.commit()
        session.close()

    else:
        results = session.query(Stats).order_by(Stats.last_updated.desc())
        last_updated = results[0].last_updated.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+"Z"
        current_datetime = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]+"Z"

        num_sg_orders = results[0].num_sg_orders
        num_mg_orders = results[0].num_mg_orders
        cur_sg_order = results[0].cur_sg_order
        cur_mg_order = results[0].cur_mg_order

        sg_req = requests.get(app_config["eventstore"]["url"] + "/set_groceries?timestamp=" + last_updated + "&end_timestamp=" + current_datetime)
        if sg_req.status_code != 200:
            logger.error("Status code for set groceries not 200")
        else:
            print("first")
            counter = 0
            for obj in sg_req.json():
                print("second")
                counter += 1
                num_sg_orders += 1
                print(num_sg_orders)
                # sg_counter += 1
                logger.debug(f"Trace ID: {obj['trace_id']}")
            logger.info(f"Number of set grocery events received: {counter}")
            cur_sg_order = counter

        mg_req = requests.get(app_config["eventstore"]["url"] + "/manual_groceries?timestamp=" + last_updated + "&end_timestamp=" + current_datetime)
        if mg_req.status_code != 200:
            logger.error("Status code for manual groceries not 200")
        else:
            counter = 0
            for obj in mg_req.json():
                counter += 1
                num_mg_orders += 1
                # mg_counter += 1
                logger.debug(f"Trace ID: {obj['trace_id']}")
            logger.info(f"Number of manual grocery events received: {counter}")
            cur_mg_order = counter

        current_datetime = datetime.datetime.strptime(current_datetime, "%Y-%m-%dT%H:%M:%S.%fZ")
        s = Stats(num_sg_orders,
                  cur_sg_order,
                  num_mg_orders,
                  cur_mg_order,
                  current_datetime)

        session.add(s)
        logger.debug(f"Number of set grocery orders: {num_sg_orders}, Max set grocery orders: {cur_sg_order}"
                     f"Number of manual grocery orders: {num_mg_orders}, Max manual grocery orders{cur_mg_order}")
        session.commit()

        session.close()

        logger.info("Processing period has ended.")

    return NoContent, 201


def init_scheduler():
    sched = BackgroundScheduler(daemon=True)
    sched.add_job(populate_stats,
                  'interval',
                  seconds=app_config['scheduler']['period_sec'])
    sched.start()


app = connexion.FlaskApp(__name__, specification_dir='')
CORS(app.app)
app.app.config['CORS_HEADERS'] = 'Content-Type'

app.add_api("grocery.yaml", strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    init_scheduler()
    app.run(port=8100, use_reloader=False)
