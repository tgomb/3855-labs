from sqlalchemy import Column, Integer, String, DateTime
from base import Base


class Stats(Base):
    """processing statistics"""
    __tablename__ = "stats"
    id = Column(Integer, primary_key=True)
    num_sg_orders = Column(Integer, nullable=False)
    cur_sg_order = Column(Integer, nullable=False)
    num_mg_orders = Column(Integer, nullable=False)
    cur_mg_order = Column(Integer, nullable=False)
    last_updated = Column(DateTime, nullable=False)

    def __init__(self, num_sg_orders, cur_sg_order, num_mg_orders, cur_mg_order, last_updated):
        """initializes a processing statistics object"""
        self.num_sg_orders = num_sg_orders
        self.cur_sg_order = cur_sg_order
        self.num_mg_orders = num_mg_orders
        self.cur_mg_order = cur_mg_order
        self.last_updated = last_updated

    def to_dict(self):
        """dictionary representation of a statistics"""
        dict = {}
        dict['id'] = self.id
        dict['num_sg_orders'] = self.num_sg_orders
        dict['cur_sg_order'] = self.cur_sg_order
        dict['num_mg_orders'] = self.num_mg_orders
        dict['cur_mg_order'] = self.cur_mg_order
        dict['last_updated'] = self.last_updated.strftime("%Y-%m-%dT%H:%M:%S")
        return dict


