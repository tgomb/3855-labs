import sqlite3

conn = sqlite3.connect('grocery.sqlite')

c = conn.cursor()
c.execute('''
          CREATE TABLE set_grocery
          (id INTEGER PRIMARY KEY ASC, 
            cost REAL NOT NULL,
            grocery_id VARCHAR(250) NOT NULL,
            date VARCHAR(250) NOT NULL,
            user_id INTEGER NOT NULL,
            timestamp VARCHAR(100) NOT NULL)
          ''')

c.execute('''
          CREATE TABLE manual_grocery
          (id INTEGER PRIMARY KEY ASC, 
           cost REAL NOT NULL,
           grocery_id VARCHAR(250) NOT NULL,
           items INTEGER NOT NULL,
           user_id INTEGER NOT NULL,
           timestamp VARCHAR(100) NOT NULL)
          ''')

conn.commit()
conn.close()
