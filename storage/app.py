import connexion
from connexion import NoContent
from mysql import connector

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from base import Base
from set_grocery import SetGrocery
from manual_grocery import ManualGrocery
import datetime
import pymysql
import yaml
import logging.config
import logging
import json
from pykafka import KafkaClient
from pykafka.common import OffsetType
from threading import Thread
from sqlalchemy import and_
import time
import os

if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yaml"
    log_conf_file = "/config/log_conf.yaml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yaml"
    log_conf_file = "log_conf.yaml"

with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

# External Logging Configuration
with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

user = app_config["datastore"]["user"]
password = app_config["datastore"]["password"]
hostname = app_config["datastore"]["hostname"]
port = app_config["datastore"]["port"]
db = app_config["datastore"]["db"]

# DB_ENGINE = create_engine("sqlite:///grocery.sqlite")
DB_ENGINE = create_engine(f'mysql+pymysql://{user}:{password}@{hostname}:{port}/{db}')
Base.metadata.bind = DB_ENGINE
Base.metadata.create_all(DB_ENGINE)
DB_SESSION = sessionmaker(bind=DB_ENGINE)

logger.info(f'Connecting to DB. Hostname: {hostname}, Port: {port}')


def get_health():
    return 200
    

def order_set(body):
    """  """

    session = DB_SESSION()

    sg = SetGrocery(body['cost'],
                    body['date'],
                    body['grocery_Set_id'],
                    body['user_id'],
                    body['timestamp'],
                    body['trace_id'])

    session.add(sg)

    logger.debug(f'Stored event set_groceries request with a trace id of {sg.trace_id}')

    session.commit()
    session.close()

    return NoContent, 201


def order_manual(body):
    """ """
    session = DB_SESSION()

    mg = ManualGrocery(body['cost'],
                       body['grocery_id'],
                       body['items'],
                       body['user_id'],
                       body['timestamp'],
                       body['trace_id'])

    session.add(mg)

    logger.debug(f'Stored event manual_groceries request with a trace id of {mg.trace_id}')

    session.commit()
    session.close()

    return NoContent, 201


def get_order_set(timestamp, end_timestamp):
    """gets new set grocery order"""
    session = DB_SESSION()

    timestamp_datetime = datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")
    end_timestamp = datetime.datetime.strptime(end_timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")
    readings = session.query(SetGrocery).filter(SetGrocery.date_created >= timestamp_datetime,
                                                SetGrocery.date_created < end_timestamp)

    results_list = []

    for reading in readings:
        results_list.append(reading.to_dict())

    session.close()
    logger.info(f"Query for Set Grocery Order after {timestamp} returns {len(results_list)} results")

    return results_list, 200


def get_order_manual(timestamp, end_timestamp):
    """gets new set grocery order"""
    session = DB_SESSION()

    timestamp_datetime = datetime.datetime.strptime(timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")
    end_timestamp = datetime.datetime.strptime(end_timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")
    readings = session.query(ManualGrocery).filter(ManualGrocery.date_created >= timestamp_datetime, 
                                                    ManualGrocery.date_created < end_timestamp)

    results_list = []

    for reading in readings:
        results_list.append(reading.to_dict())

    session.close()
    logger.info(f"Query for Manual Grocery Order after {timestamp} returns {len(results_list)} results")

    return results_list, 200


def process_messages():
    """ Process event messages """
    k_hostname = "%s:%d" % (app_config['events']['hostname'],
                            app_config['events']['port'])
    retries = app_config["retries"]["max"]
    sleep_time = app_config["retries"]["sleep"]
    
    try_counter = 0

    while try_counter < retries:
        logger.info(f"Attempting to connect to Kafka - Current retry count: {try_counter}")
        try:
            client = KafkaClient(hosts=k_hostname)
            topic = client.topics[str.encode(app_config['events']['topic'])]

            consumer = topic.get_simple_consumer(consumer_group=b'event_group',
                                                reset_offset_on_start=False,
                                                auto_offset_reset=OffsetType.LATEST)
            for msg in consumer:
                msg_str = msg.value.decode('utf-8')
                msg = json.loads(msg_str)
                logger.info('Message: %s' % msg)

                payload = msg['payload']
                # print(payload)

                if msg['type'] == 'sg':
                    # store the event 1 (payload) to the DB just store it LOL
                    # logger.info(f" inside set: {msg}")
                    order_set(payload)

                elif msg['type'] == 'mg':
                    # store the event 2 (payload) to the DB
                    # logger.info(f"inside manuel :^) {msg}")
                    order_manual(payload)

                consumer.commit_offsets()

        except: 
            logger.error("Error connecting to Kafka")
            try_counter += 1
            time.sleep(sleep_time)


app = connexion.FlaskApp(__name__, specification_dir='')
app.add_api("grocery.yaml", strict_validation=True, validate_responses=True)

if __name__ == "__main__":
    t1 = Thread(target=process_messages)
    t1.setDaemon(True)
    t1.start()
    app.run(port=8090)

