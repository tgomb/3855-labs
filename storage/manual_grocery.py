from sqlalchemy import Column, Integer, String, DateTime, Float
from base import Base
import datetime


class ManualGrocery(Base):
    """  """

    __tablename__ = "manual_grocery"

    id = Column(Integer, primary_key=True)
    cost = Column(Float, nullable=False)
    grocery_id = Column(String(250), nullable=False)
    items = Column(Integer, nullable=False)
    user_id = Column(Integer, nullable=False)
    timestamp = Column(DateTime, nullable=False)
    date_created = Column(DateTime, nullable=False)
    trace_id = Column(Integer, nullable=False)

    def __init__(self, cost, grocery_id, items, user_id, timestamp, trace_id):
        """  """
        self.cost = cost
        self.grocery_id = grocery_id
        self.items = items
        self.user_id = user_id
        self.timestamp = timestamp
        self.date_created = datetime.datetime.now()  # Sets the date/time record is created
        self.trace_id = trace_id

    def to_dict(self):
        """   """
        dict = {}
        dict['id'] = self.id
        dict['cost'] = self.cost
        dict['grocery_id'] = self.grocery_id
        dict['items'] = self.items
        dict['user_id'] = self.user_id
        dict['timestamp'] = self.timestamp
        dict['date_created'] = self.date_created
        dict['trace_id'] = self.trace_id

        return dict

