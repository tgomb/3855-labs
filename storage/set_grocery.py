from sqlalchemy import Column, Integer, String, DateTime, Float
from base import Base
import datetime


class SetGrocery(Base):
    """  """

    __tablename__ = "set_grocery"

    id = Column(Integer, primary_key=True)
    cost = Column(Float, nullable=False)
    grocery_Set_id = Column(String(250), nullable=False)
    date = Column(String(250), nullable=False)
    user_id = Column(Integer, nullable=False)
    timestamp = Column(DateTime, nullable=False)
    date_created = Column(DateTime, nullable=False)
    trace_id = Column(Integer, nullable=False)

    def __init__(self, cost, grocery_Set_id, date, user_id, timestamp, trace_id):
        """  """
        self.cost = cost
        self.grocery_Set_id = grocery_Set_id
        self.date = date
        self.user_id = user_id
        self.timestamp = timestamp
        self.date_created = datetime.datetime.now()  # Sets the date/time record is created
        self.trace_id = trace_id

    def to_dict(self):
        """   """
        dict = {}
        dict['id'] = self.id
        dict['cost'] = self.cost
        dict['grocery_Set_id'] = self.grocery_Set_id
        dict['date'] = self.date
        dict['user_id'] = self.user_id
        dict['timestamp'] = self.timestamp
        dict['date_created'] = self.date_created
        dict['trace_id'] = self.trace_id

        return dict

