import mysql.connector
import yaml

with open('app_conf.yaml', 'r') as f:
    app_config = yaml.safe_load(f.read())

user = app_config["datastore"]["user"]
password = app_config["datastore"]["password"]
hostname = app_config["datastore"]["hostname"]
port = app_config["datastore"]["port"]
db = app_config["datastore"]["db"]

db_conn = mysql.connector.connect(host=hostname, user=user, password=password, database=db)

db_cursor = db_conn.cursor()

db_cursor.execute('''
          CREATE TABLE set_grocery
          (id INT NOT NULL AUTO_INCREMENT, 
            cost FLOAT NOT NULL,
            grocery_Set_id VARCHAR(250) NOT NULL,
            date VARCHAR(250) NOT NULL,
            user_id INT NOT NULL,
            timestamp VARCHAR(100) NOT NULL,
            trace_id VARCHAR(100) NOT NULL,
            date_created VARCHAR(100) NOT NULL,
            CONSTRAINT set_groceries_id_pk PRIMARY KEY (id))
          ''')

db_cursor.execute('''
          CREATE TABLE manual_grocery
          (id INT NOT NULL AUTO_INCREMENT, 
           cost FLOAT NOT NULL,
           grocery_id VARCHAR(250) NOT NULL,
           items INT NOT NULL,
           user_id INT NOT NULL,
           timestamp VARCHAR(100) NOT NULL,
           trace_id VARCHAR(100) NOT NULL,
           date_created VARCHAR(100) NOT NULL,
           CONSTRAINT manual_groceries_id_pk PRIMARY KEY (id))
          ''')

db_conn.commit()
db_conn.close()
