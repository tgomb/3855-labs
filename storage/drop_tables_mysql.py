import mysql.connector
import yaml

with open('app_conf.yaml', 'r') as f:
    app_config = yaml.safe_load(f.read())

user = app_config["datastore"]["user"]
password = app_config["datastore"]["password"]
hostname = app_config["datastore"]["hostname"]
port = app_config["datastore"]["port"]
db = app_config["datastore"]["db"]


db_conn = mysql.connector.connect(host=hostname, user=user, password=password, database=db)

db_cursor = db_conn.cursor()

db_cursor.execute('''
DROP TABLES set_grocery, manual_grocery
''')

db_conn.commit()
db_conn.close()
