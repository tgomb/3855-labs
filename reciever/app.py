import datetime
import json
import logging.config
import random
import logging
import connexion
import requests
import yaml
from connexion import NoContent
import datetime
from pykafka import KafkaClient
import time
import os

MAX_EVENTS = 10
EVENT_FILE = 'events.json'
event_data = []
headers = {'Content-Type': 'application/json'}

# caching the data and not reading the file means the data gets deleted when you restart the app
if "TARGET_ENV" in os.environ and os.environ["TARGET_ENV"] == "test":
    print("In Test Environment")
    app_conf_file = "/config/app_conf.yaml"
    log_conf_file = "/config/log_conf.yaml"
else:
    print("In Dev Environment")
    app_conf_file = "app_conf.yaml"
    log_conf_file = "log_conf.yaml"

# opening YAML files
with open(app_conf_file, 'r') as f:
    app_config = yaml.safe_load(f.read())

# External Logging Configuration
with open(log_conf_file, 'r') as f:
    log_config = yaml.safe_load(f.read())
    logging.config.dictConfig(log_config)

logger = logging.getLogger('basicLogger')

logger.info("App Conf File: %s" % app_conf_file)
logger.info("Log Conf File: %s" % log_conf_file)

kafka_hostname = app_config['events']['hostname']
kafka_port = app_config['events']['port']
kafka_topic = app_config['events']['topic']

retries = app_config["retries"]["max"]
sleep_time = app_config["retries"]["sleep"]

try_counter = 0

while try_counter < retries:
    logger.info(f"Attempting to connect to Kafka - Current retry count: {try_counter}")
    try:
        client = KafkaClient(hosts=f"{kafka_hostname}:{kafka_port}")
        topic = client.topics[str.encode(kafka_topic)]
        producer = topic.get_sync_producer()
        break

    except:
            logger.error("Error connecting to Kafka")
            try_counter += 1
            time.sleep(sleep_time)


def get_health():
    return 200
    

def order_set(body):
    """receives an order for the preset grocery list"""

    # curr_datetime = datetime.datetime.now()
    # curr_datetime_str = curr_datetime.strftime('%Y-%m-%d %H:%M:%S')
    #
    # new_request = {'received_timestamp': '', 'request': ''}  # setting up the json data structure
    #
    # received_timestamp = curr_datetime_str

    #  redundant basically converting from dict to string back to dict
    python_data = json.dumps(body)  # reads the request data
    json_load = json.loads(python_data)  # converts it to python json data
    # cost = json_load['cost']  # taking variables form the request
    # grocery_Set_id = json_load['grocery_Set_id']  # taking variables from the request
    # request = f"The Grocery items ID is: {grocery_Set_id} and costs: ${cost}"

    # new_request['received_timestamp'] = received_timestamp  #
    # new_request['request'] = request

    #  can make into another function to reduce repetition
    # if len(event_data) == MAX_EVENTS:  # checks if the file length is 10
    #     event_data.pop(-1)  # if length is 10 remove the first element
    #     event_data.insert(0, new_request)  # then insert the new data
    # else:
    #     event_data.insert(0, new_request)  # if the file isn't full, insert the data
    #
    # fh = open(EVENT_FILE, 'w')  # open the file as 'write'
    # json.dump(event_data, fh)
    # fh.close()

    # db_request = {'cost': json_load['cost'], 'date': json_load['date'], 'grocery_Set_id': json_load['grocery_Set_id'],
    #               'user_id': json_load['user_id']}
    trace_id = random.randint(1, 999999999)
    json_load['trace_id'] = trace_id

    logger.info(f'Received event set_groceries with a trace id of {trace_id}')

    # requests.post("http://localhost:8090/set_groceries", json=db_request, headers=headers)
    # x = requests.post(app_config['eventstore1']['url'], json=json_load, headers=headers)

    # kafka_hostname = app_config['events']['hostname']
    # kafka_port = app_config['events']['port']
    # kafka_topic = app_config['events']['topic']
    # client = KafkaClient(hosts=f"{kafka_hostname}:{kafka_port}")
    # topic = client.topics[str.encode(kafka_topic)]
    # producer = topic.get_sync_producer()
    
    msg = { "type": "sg",
            "datetime": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
            "payload": json_load}
    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))
    # logging
    logger.info(f'Returned event set_groceries response id: {trace_id} with status 201')

    return NoContent, 201


def order_manual(body):
    """receives manual order for groceries"""

    # curr_datetime = datetime.datetime.now()
    # curr_datetime_str = curr_datetime.strftime('%Y-%m-%d %H:%M:%S')
    #
    # new_request = {'received_timestamp': '', 'request': ''}
    # received_timestamp = curr_datetime_str

    python_data = json.dumps(body)
    json_load = json.loads(python_data)
    # cost = json_load['cost']
    # items = json_load['items']
    # request = f"The amount of items ordered are: {items} and costs: ${cost}"

    # new_request['received_timestamp'] = received_timestamp
    # new_request['request'] = request

    #  can make into another function to reduce repetition
    # if len(event_data) == MAX_EVENTS:
    #     event_data.pop(-1)
    #     event_data.insert(0, new_request)
    # else:
    #     event_data.insert(0, new_request)
    #
    # fh = open(EVENT_FILE, 'w')
    # json.dump(event_data, fh)
    # fh.close()
    # db_request = {'cost': json_load['cost'], 'grocery_id': json_load['grocery_id'], 'items': json_load['items'],
    #               'user_id': json_load['user_id']}
    trace_id = random.randint(1, 999999999)
    json_load['trace_id'] = trace_id

    logger.info(f'Received event manual_groceries with a trace id of {trace_id}')

    # requests.post("http://localhost:8090/manual_groceries", json=db_request, headers=headers)
    # x = requests.post(app_config['eventstore2']['url'], json=json_load, headers=headers)

    # kafka_hostname = app_config['events']['hostname']
    # kafka_port = app_config['events']['port']
    # kafka_topic = app_config['events']['topic']
    # client = KafkaClient(hosts=f"{kafka_hostname}:{kafka_port}")
    # topic = client.topics[str.encode(kafka_topic)]
    # producer = topic.get_sync_producer()
    msg = { "type": "mg",
            "datetime": datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S"),
            "payload": json_load}
    msg_str = json.dumps(msg)
    producer.produce(msg_str.encode('utf-8'))

    # logging
    logger.info(f'Returned event manual_groceries response id: {trace_id} with status 201')

    return NoContent, 201


app = connexion.FlaskApp(__name__, specification_dir="")
app.add_api("grocery.yaml",
            strict_validation=True,
            validate_responses=True)

if __name__ == "__main__":
    app.run(port=8080)
